package tv.codely.java_bootstrap;

public class AgedBrie extends UpdateTableItem {

    private static final int MIN_SELLIN =  0;

    AgedBrie(Item item){
        super(item);

    }

    @Override
    void update(){
        decreaseSellIn();

        if(item.sellIn < MIN_SELLIN ){
            item.quality = item.quality - item.quality;
        }
    }
}
