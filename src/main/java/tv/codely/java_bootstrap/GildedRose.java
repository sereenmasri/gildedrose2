package tv.codely.java_bootstrap;

class GildedRose {

    private Item[] items;

    GildedRose(Item[] items) {
        this.items = items;
    }

    void updateQuality() {
        for (Item item : items) {
            UpdateTableItem updateTableItem = UpdateItemFactory.create(item);
            updateTableItem.update();
        }
    }
 }

