package tv.codely.java_bootstrap;



public class StandardItem extends UpdateTableItem {

    private static final int MAX_QUALITY = 50;


    StandardItem(Item item){
        super(item);

    }

    @Override
    void update() {

    }

    public void decreaseSellIn(){
        item.sellIn -= 1;

    }

    void increaseQuality(){
        if (item.quality < MAX_QUALITY){
            item.sellIn += 1;
        }
    }

}
