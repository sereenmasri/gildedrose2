package tv.codely.java_bootstrap;

abstract class UpdateTableItem extends Item{


    private static final int MAX_QUALITY = 50;
    private static final int MIN_QUALITY = 0;

    final Item item;

    public UpdateTableItem(Item item){
        super(item.name, item.sellIn, item.quality);
        this.item = item;

    }


    abstract void update();

    public void decreaseSellIn(){
        item.sellIn -= 1;

    }

    void increaseQuality(){
       if (item.quality < MAX_QUALITY){
           item.sellIn =+ 1;
       }
    }

    void decreaseQuality(){
        if (item.sellIn < 0){
            item.quality =- 1;
        }
    }


}
